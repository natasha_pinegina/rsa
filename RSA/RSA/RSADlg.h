﻿
// RSADlg.h: файл заголовка
//

#pragma once


// Диалоговое окно CRSADlg
class CRSADlg : public CDialogEx
{
// Создание
public:
	CRSADlg(CWnd* pParent = nullptr);	// стандартный конструктор

// Данные диалогового окна
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_RSA_DIALOG };
#endif

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// поддержка DDX/DDV


// Реализация
protected:
	HICON m_hIcon;

	// Созданные функции схемы сообщений
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedGenerate();
	/*unsigned*/ long  int E;
	/*unsigned*/ long int D;
	CString Init;
	CString Get;
	afx_msg void OnBnClickedDownload();
	afx_msg void OnBnClickedGenerate2();
	afx_msg void OnBnClickedDownload2();
};
