﻿
// RSADlg.cpp: файл реализации
//

#include "pch.h"
#include "framework.h"
#include "RSA.h"
#include "RSADlg.h"
#include "afxdialogex.h"
#include <string>
#include <fstream>
#include <sstream>
#include <iostream>
#include <vector>


#ifdef _DEBUG
#define new DEBUG_NEW
#endif
#define RAND_MAX  65536
using namespace std;
// Диалоговое окно CRSADlg



CRSADlg::CRSADlg(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_RSA_DIALOG, pParent)
	, E(0)
	, D(0)
	, Init(_T(""))
	, Get(_T(""))
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CRSADlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_OpenKey, E);
	DDX_Text(pDX, IDC_CloseKey, D);
	DDX_Text(pDX, IDC_Init, Init);
	DDX_Text(pDX, IDC_Get, Get);
}

BEGIN_MESSAGE_MAP(CRSADlg, CDialogEx)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_Generate, &CRSADlg::OnBnClickedGenerate)
	ON_BN_CLICKED(IDC_Download, &CRSADlg::OnBnClickedDownload)
	ON_BN_CLICKED(IDC_Generate2, &CRSADlg::OnBnClickedGenerate2)
	ON_BN_CLICKED(IDC_Download2, &CRSADlg::OnBnClickedDownload2)
END_MESSAGE_MAP()


// Обработчики сообщений CRSADlg

BOOL CRSADlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// Задает значок для этого диалогового окна.  Среда делает это автоматически,
	//  если главное окно приложения не является диалоговым
	SetIcon(m_hIcon, TRUE);			// Крупный значок
	SetIcon(m_hIcon, FALSE);		// Мелкий значок

	// TODO: добавьте дополнительную инициализацию

	return TRUE;  // возврат значения TRUE, если фокус не передан элементу управления
}

// При добавлении кнопки свертывания в диалоговое окно нужно воспользоваться приведенным ниже кодом,
//  чтобы нарисовать значок.  Для приложений MFC, использующих модель документов или представлений,
//  это автоматически выполняется рабочей областью.

void CRSADlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // контекст устройства для рисования

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Выравнивание значка по центру клиентского прямоугольника
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Нарисуйте значок
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

// Система вызывает эту функцию для получения отображения курсора при перемещении
//  свернутого окна.
HCURSOR CRSADlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

//Быстрое возведение в степень по модулю
/*unsigned*/long int mul(long long a, long long b, long long m)
{
	if (b == 1)
		return a;
	if (b % 2 == 0) {
		long long t = mul(a, b / 2, m);
		return (2 * t) % m;
	}
	return (mul(a, b - 1, m) + a) % m;
}

/*unsignedlong*/ int pows(long long  a, int  b, long long   m)
{
	if (b == 0)
		return 1;
	if (b % 2 == 0) {
		long long t = pows(a, b / 2, m);
		return mul(t, t, m) % m;
	}
	return (mul(pows(a, b - 1, m), a, m)) % m;
}
//НОД
/*unsigned*/ long int gcd(long long a, long long b) {
	if (b == 0)
		return a;
	return gcd(b, a % b);
}
//тест Ферма
bool ferma(unsigned int x)
{
	if (x == 2)
		return true;
	//srand(time(NULL));
	for (int i = 0; i < 10; i++)
	{
		long long a = (rand() % (x - 2)) + 2;
		if (gcd(a, x) != 1)
			return false;
		if (pows(a, x - 1, x) != 1)
			return false;
	}
	return true;
}

/*unsigned*/ long int p = 0, q = 0;

void findP()
{
	srand(time(NULL));
	int t = 0;
	/*unsigned long int delta = 0;*/
	while(p==0 /*&& q==0*/)
	{ 
	/*unsigned*/long int P = rand();
	/*unsignedlong int Q = rand();*/
	/*if (P > Q)delta = P - Q;
	else delta = Q - P;*/
	if (/*delta < 3000 && */ferma(P) == true/* && ferma(Q) == true*/)
	{
		p = P;
		//q = Q;
	}
	t++;
	}
	
}
void findQ()
{
	//srand(time(0));
	unsigned long int delta = 0;
	int t = 0;
	while (/*p == 0 &&*/ q==0)
	{
		/*unsignedlong int P = rand();*/
		/*unsigned*/long int Q = rand();
		if (p > Q)delta = p - Q;
		else delta = Q - p;
		if (delta < 3000 && ferma(Q) == true && Q!=p)
		{
			//p = P;
			q = Q;
		}
		t++;
	}
}

long int rash_gcd(/*unsigned*/long int a, /*unsigned*/long int b, /*unsigned*/long int& x, /*unsigned*/long int& y)
{
	if (a == 0) 
	{
		x = 0; y = 1;
		return b;
	}
	/*unsigned*/ long int x1, y1;
	long int d = rash_gcd(b % a, a, x1, y1);
	x = y1 - (b / a) * x1;
	y = x1;
	return d;
}

/*unsigned*/long int n = 0;
string res_new;
string res;

void CRSADlg::OnBnClickedGenerate()
{
	UpdateData(true);
	res = "";
	Get = " ";
	string Res;
	//res_new = "";
	string data;
	data.resize(Init.GetLength());
	WideCharToMultiByte(CP_ACP, 0, Init, -1, &data[0], data.size(), NULL, NULL);
	unsigned char symbol1 = ' ';
	unsigned char symbol2 = ' ';
	Res = "";
	int i = 0;
	for (unsigned char c : data)		//цикл, пробегающий по каждому символу текста
	{
		 int M = (int)c;
		//int M = (static_cast<int>(data[j]));
		/*unsigned/*long */long  int C = pows(M, E, n);
		///*unsigned*//*long*/ int M_new = pows(C, D, n);
		//res += (int)C ;
		stringstream str;
		str << C;
		str>>Res;
		res += Res+" ";
		//res += to_string(C);
	}
	Get = res.c_str();
	UpdateData(false);
	
}


void CRSADlg::OnBnClickedDownload()
{
	CFileDialog fileDialog(TRUE, NULL, L"*.txt");
	int res = fileDialog.DoModal();
	if (res != IDOK)
		return;
	CFile file;
	file.Open(fileDialog.GetPathName(), CFile::modeRead);
	CStringA str;
	LPSTR pBuf = str.GetBuffer(file.GetLength() + 1);
	file.Read(pBuf, file.GetLength() + 1);
	pBuf[file.GetLength()] = NULL;
	//CStringA decodedText = str;
	Init = str;
	file.Close();
	str.ReleaseBuffer();
	UpdateData(FALSE);
}


void CRSADlg::OnBnClickedGenerate2()
{
	Init = " ";
	string res_new="";

	string data_new;
	data_new.resize(Get.GetLength());
	WideCharToMultiByte(CP_ACP, 0, Get, -1, &data_new[0], data_new.size(), NULL, NULL);
	
	/*unsigned char */ string symbol;
	int d = 0;
	d = D;
	int i = 0,j=0;
	//stringstream str;
	vector<string> arr;
	//for (unsigned char c : data_new)		//цикл, пробегающий по каждому символу текста
	//{
		for (;i < data_new.length();)		//цикл, пробегающий по каждому символу текста
		{
			symbol = "";
			stringstream str;
			while (data_new[i] != ' ')
			{
				str << data_new[i];
				i++;
			}
			i++;
			str >> symbol;

			long int M = pows(atoi(symbol.c_str()), d, n);
			res_new += (char)M;
		}
	//}
	Init = res_new.c_str();
	UpdateData(false);
}


void CRSADlg::OnBnClickedDownload2()
{
	MessageBox(L"Подождите",L"Ключи формируются!", MB_OK);
	do {
		findP();
		findQ();
		n = p * q;
		//Значение функции Эйлера
		long int Phi = (p - 1) * (q - 1);
		long int e = rand();
		while (gcd(e, Phi) != 1 && e < Phi)
		{
			e = rand();
		}
		E = e;
		UpdateData(false);
		long int d = 0, alfa = 0, beta = 0;
		rash_gcd(e, Phi, alfa, beta);
		d = alfa;
		D = d;
	} while (D < 0);
	UpdateData(false);
}
